When we approach the development of a new product we follow a specific workflow and some general guidelines, all ideas must be validated using this process before to decide if they are worth developing. 

# Guidelines

- Product Management is not easy and it is costly: the last thing that we want is coding a MVP.  

- Product Management is not about coding and is not for geeks: Development will come at the end. 

- Act fast think fast: do not discuss too much details within validation process, we need to go fast 

- Do not overplan: "A good plan violently executed now is better then a perfect plan next week" (General George S. Patton) ...generals have hundred and hundred of years of experience in managing plans and teams, we might trust them

- Assumption, not intuition: You (probably) had a good intuition, but remember there already are a lot of guys doing the same in the world: the work will now be differentiating, the first storytelling that comes after the intuition is never the good one... your intuition? let's call it assumption

- Assumption, not opinion: You will never sell a product on the basis of your opinion, the best product is driven by customer needs. Opinions are forbidden, we need Data and measures.

- Validation is mandatory, collect data and don't fall in love with your ideas.

# Process

We start always from the classical Business Model Canvas to map all aspects of our ideas and then we proceed iterating and validating our assumptions with a data driven approach involving technics like growth hacking.

This process will be executed by the Product Owner in collaboration with the Sales and Marketing team. The development team will act only in the MVP phase.


Logical Steps in the process are:

- Concept: all these steps are mandatory:
    -	What is your target market? Is it broad or niche? 
    -	You can have two main drivers that could be perceived from the customer: sell more or save cost. What is your product about? Remeber that it's harder the "Sell more" option 
    -	What painpoint do you solve? Is it perceived as a pain from the customer or is it just a nice to have? All your target has the same perception? Customer won’t pay for a nice to have 
    -	Benchmark and find how your product is different (not in terms of price) from the competition. Write down features and differentiation points 
    -	Find the killer KSP (Key Selling Point)
    -	What is your business model? Is it Freeware or what? Remember that many customers want to touch the benefit before they buy 
    -	Do you have a price assumption in your mind? 
    -	Do have any idea regarding how to name your product?

                               
* Value: Product pricing and revenues. It is really important to understand where is the value for the customer, the use case and the underlying ROI. If you don’t know these three components you are not validating the concept 


- MEVO: A MEVO is a Minimum Economic Viable Offer which represents a prototype (UX or other, but NO CODING) of your concept. Avoid coding at MEVO if it is possible.



- Customer validation: We use growth hacking process for this because it is the most part of the activity and it is very easy to overfit on a champion customer.

- Product features validation: When you have the concept validated, you might validate the features.
    - Never pitch the How, pitch the What
    - Don’t think about selling: you are not on the customer to sell something, you are validating
    - Ask advice to the customer
    - Be severe on your features
    - Validate assumptions
    - Write down validations with a customer otherwise you won’t be able to share with the team and you will forget something important
    - Validate features also with communities 
    - Avoid overselling and try to validate few features  

* Roadmap: Avoid plans, planning is better. Roadmap will evolve frequently,  but it must be clear to all internal stakeholder and Sales must understand clearly where the value is 

- MVP

- Customer Creation



We have an official template ( Product Management - template )  in the sharepoint "Products", to collect all the information in a structured way.

