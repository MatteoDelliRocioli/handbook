## Vacation Policies
Agile Lab strongly encourages people to take vacation because it is important to the health and well-being of our employees.
Every employee should really care about her/his health, joy and serenity and take some days off to clear stress out and spend a good time with the family and friends.


### Accrual
Vacation days are accrued according to the type of contract (see [Benefits](#Benefits)).

### Accumulation
Agile Lab employees are discouraged to accumulate vacations. Thus, employees should consume as much vacation days as possible before the end of the year.
The best would be consuming all accrued vacation days within the same year of accrual.

### Common sense collaboration
Employees should always plan for vacation and double check with their manager how to handle the vacation period.
We trust our employees and we expect everyone to collaborate in order to help the manager to fit everyone's need with the planned schedule.

Collaboration means making sure that, for instance, not all project members take a vacation at the same time in the middle of a delivery.

Every employee should have visibility about all the others in order to agree on a plan.

### Default allocation
Every employee has a default allocation of vacation to make sure they consume a minimum amount of days off before the end of the year in case they don't plan any vacation.

The default allocation roughly follows these slots:
- The two central weeks of August
- Two weeks between December and January

Additionally, if the amount of days of the default allocation exceeds the vacations days accumulable during the year, employees are not forced to take extra vacation but they can ask for (see [Approval](#Approval)). 

### Planning

Leaders periodically remind employees to plan for vacation.
Vacation must be planned in sufficient advance to let the leader rearrange activities and inform the customer in time:
* Sporadic days off or period within two consecutive weeks can be planned with no great notices in advance, do your best (see [Common sense collaboration](#Common-sense-collaboration)).
* 2 weeks (or longer) vacations should be planned at least three months in advance

The default allocation slots can be canceled with at least two quarters in advance and only after planning for an alternative period of vacation with the same amount of days.

### Approval

In general there is no need for approval of accrued vacation days. All accrued vacation days are automatically approved until they are planned in time.

For instance, if you want to take a vacation during the last week of August, thus you shoud notice your leader before the last week of July.
If you didn't cancel the default allocation in time, you will take three weeks of vacations.

Vacation exceeding the accrued days are subjected to approval by the leader.

### Leader duties
Leaders generate awareness. A leader is accountable to ensure that people are fine or not with the default holiday. She/he periodically (with a period up to the leader) reminds employees to allocate holidays.

A leader has the responsibility to negotiate vacation periods across all the team members to guarantee a fair distribution of days off (if possible). 

She/he has also the responsibility to inform the customer about vacation periods and plan for them.

Leaders talk to one another when people move across different circle/projects to make sure about their availability.

### Employee duties
Employees must plan vacations and inform leaders of projects and circles in time.
