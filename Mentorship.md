# Mentorship

Software Factory is accountable to keep high the quality of the software we deliver, and one way to achieve the goal is through mentorship.

### Software Mentor

The {% role %}Software Factory/Software Mentor{% endrole %} is recognized as an expert in one or more technologies and can help solve technical issues across projects/teams.

The Mentor can have a focus on specific technologies, and each Agile Lab member can request him/her help after a bunch of tries on its own (trying on your own is fundamental before asking for help). If the requested help can be better handled by other Mentors, the Mentor can forward the request to them.

### Team Mentor engagement

In addition to requesting help to a Software Mentor individually we also encourage Project Leaders to define a recurring meeting with a Software Mentor where:
* Project Lead highlights pain points of the whole team (technologies, languages, tools, etc)
* Project Lead reports individual technical issues that need to be addressed
* Software Mentor lists the mentoring sessions performed involving team members, and their topics
* Software Mentor advertises newly released training material (workshops, webinars, slides, etc)
In order to keep track of it, a checklist for both Project Leads and Software Mentors should be created.

In order to engage a Software Mentor, you can simply write an email to them explaining briefly which kind of help you need; remember: there are no off-topic requests, and you can ask for a Software Mentor for all kind of questions, from how to set up your IDE to how to improve the performances of your job.

In order to achieve this:
* Each Software Mentor reserves some time during the week to meet with the Project Leads.
* Software Mentors can ask for help of other Mentors depending on the specific knowledge area (if a mentor is skilled in AWS but not in Python, he/she should get the AWS problems and ask for other mentors to take care of the Python ones).
* Even if the Project Leader is already a skilled programmer (or a Software mentor him/herself) it is suggested to schedule meetings with other Software Mentors to get an external opinion and to spread knowledge.
* Software Mentors should anyway find at least 2 hours weekly to handle on-demand requests.

Remember that a Software Mentor is very helpful not only when specific issues require good technical skills, but can also contribute to the overall architecture and the more he/she knows the team member skills, the more he/she will have the right sensibility to explain concepts and transfer knowledge most properly.

### Why we love mentorship

Mentorship is one of the most fundamental tools to keep the quality at the top level while the company grows. The most amazing part of mentorship is that mentors are not for free, you need to invest in them. Being the Software Mentor is a responsibility that can be practiced also by junior engineers when they have a skill set that fits the selected project.
These activities enable junior engineers in improving their soft skills by mentoring other colleagues and validating the hard skills that are supposed to be already assimilated, becoming the leaders of tomorrow. Teams need mentors as much as mentors need teams.

The first experience as Project Lead can be easier if you already have prior experience as Software Mentor. A positive Software Mentor experience helps you to climb the career ladder.