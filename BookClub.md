# Book Club

The Book Club is an initiative of the Training Circle. It is run by the {% role %}Training/Book Club curator{% endrole %}.

## FAQ

### What is the Book Club?
The Book Club is a community where we read to expand our knowledge and inspire knowledge sharing.

### What is the goal of the Book Club?
The goal of the Book Club is to promote:
- learning something new (continuous learning)
- learning something useful (real-world applications)
- learning from each other (discussion).

### What do we do at the Book Club?
Every two weeks,
- we read something new (eg book chapter, blog post) in async,
- we collect discussion points in async, based on a discussion template,
- we meet (30min) to discuss what we read.

### How does the discussion work?
In the discussion, we review the discussion points that were collected in async. This means we go through the submitted points, and eg questions are answered, lessons learned/tips are presented.

To maximize info exchange in the discussion, the discussion points are based on a discussion template.

### How do we choose our next book?
When a book is completed, we choose a new book.
1. The Book Club creator collects strategic topics for continuous learning from the Software Development Circle.
2. The Book Club Curator selects at least a book for each strategic topic.
3. The Book Club Curator shares the selected books in a poll to all participants of the Engine Circle.
4. All interested readers vote the book(s) they are interested to read in the Book Club, based on current personal interest/goals.
5. The Book Club Curator communicates the chosen book - meaning the top voted book - to all participants of the Engine Circle. 
6. The Book Club Curator adds any new interested reader to the Book Club Teams group. 
7. The Book Club Curator communicates next steps on the Book Club Teams group, including book purchasing details, reading scope for the first session.

### How can I join the Book Club?
You can join the Book Club and start reading with us at any time.
- Please check on Holaspirit who currently energizes the role of {% role %}Training/Book Club curator{% endrole %}.
- Write a message to the {% role %}Training/Book Club curator{% endrole %} and ask to be added to the Book Club.

#### How can I buy the current book?
When joining the Book Club, you can purchase the current book and get reimbursement.

- Please check on Holaspirit who currently energizes the role of {% role %}Training/Book Club curator{% endrole %}.
- Write a message to the {% role %}Training/Book Club curator{% endrole %} and ask what is the current book.
- Choose one of the purchase options below:

**Option 1**: You can buy the digital book and get full reimbursement.
	On Elapseit, record *one* expense: 
    
    1) Use the project: `<current year> Agile - Training - Book Club Curator`
	
**Option 2**: You can buy the paperback book and get full reimbursement.
	On Elapseit, record *two* expenses:
    
    1) one expense of 23,29 euro (the price of the current digital book).
		Use the project: `<current year> Agile - Training - Book Club Curator`
	
    2) one expense with the remaining amount. 
		Use the project: `<current year> Agile - Training - Learning`

In Option 2 above, expense 2 is a personal expense from your learning budget. For more info, see Traning Budget in [Benefits](Benefits.md).

For more info about the general purchasing process, see scenario 1 in [Spending Company Money](SpendingCompanyMoney.md).

#### Can I use company time for Book Club activities?
Yes, you can! You can safely use company time (Training time budget) for our sessions.
	
If needed, you can choose to use company time (Training time budget) for reading and preparing for the session.

This means you need to track Book Club activities on ElapseIt as explained in the table below:

| Activity | Project on ElapseIt |
|----------|---------------------|
|Reading outside office hours|Nothing to track|
|Reading during office hours|`<current year> Agile - Training - Learning`|
|Session (during office hours)|`<current year> Agile - Training - Learning`|

For more info about Training time budget, see Training Budget in [Benefits](Benefits.md).
