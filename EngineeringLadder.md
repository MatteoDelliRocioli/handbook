The Engineering Ladder is our way to provide people a professional growth path. 
What is needed to advance to the next level and get a promotion is well explained and explicit (transparency).

Our Path is composed of seven levels. The first four are the same for everyone, while the last three are different for tech and manager tracks, as shown in the below image.

![Ladder](images/ladder.png)

The Engineering ladder is not related to self-management roles. Self-management roles are needed by the organization, while each individual requires ladder levels to grow as a professional. It helps to develop skills across four main directions:
* Dex - Technical skills
* Str - Get Stuff Done
* Wis - Impact
* Cha - Communication & Leadership

The extended document is located in BigData Sharepoint / Career Ladder and it will be updated and releases every six months by the `role:Engine/COO`

This ladder was built taking inspiration mainly from the "rent the runaway" ladder and many others. Thanks



### Rules

* Expectations at each level are cumulative
*  Dex and Str ( the «adder» attributes) are the primary drivers of promotions at lower levels. At higher levels, the 
importance gradually shifts to Wis and Cha ( the «multiplier» attributes )
• Not everyone will progress in each column in lockstep. If you are Dex:6/Cha:3; you will not be stuck at L3, but you will likely not be promoted all the way L6
• To advance to the next level, you need to consistently perform at that level for some time (6M – 1Y) and then 
needed by the organization
* To reach Engineer 3, a positive experience as Project/Team leader is helpful
* Each level has a compensation range associated. If you are stuck in a level, you can anyway move forward your compensation, but only up to the upper limit.
* Each level has a quota, calculated by the engine lead link based on the total number of people in the Engine department and their distribution across levels.
* Ladder levels are not applied to external consultants hired by other companies. Those instead we hire remotely are included in this mechanism.


### How to get promoted

* When is possible we approach things to bottom-up and with self-responsibility in mind
* If you want to be promoted, you need to assess yourself and pitch for it
* The first person to convince is your coach ( project leader/lead link )
* Ask your coach to assess if there is quota into the next level.
* Your coach  will mentor you on this, helping to build a dossier to support your pitch
* Pitch will be done to a specific committee.

The coach has a huge responsibility in this process:
- if a candidate is not ready to apply for the next level and the coach does not detect it, it can cause a huge demotivation effect in case it is not promoted
- If a candidate is ready, but is not self-confident to apply for the promotion, could be demotivating as well because no-one is recognizing its value


### The dossier

Not all the people in the committee are aware of what the candidate is doing and how it is performing. The ladder is composed by a list if expectations that the candidate needs to fulfill. Most of them are soft or not easy to demonstrate in an interview, so the candidate should build a dossier where for each expectation is showing in which way and why it is compliant with that. It is also a matter of transparency.
The dossier should be a collection of sharable documents . The candidate needs to share them with the committee at least one week before the pitch, so they can prepare questions and can have a clear overview about it.
The documents should be self-explanatory, linked to the ladder's items and should also demonstrate that the candidate is acting at the target level since 6-12 months with consistency.
The dossier should also include a presentation of the candidate's path in Agile Lab ( which projects have been done and what have been have learned, etc ).


### Promotion Committee

We want to distribute the accountability and don't let a single person judge for a promotion, limiting the judgment bias and the candidate's frustration against a single person.

The following roles will compose the committee:
- Lead link of the Architecture & Research circle
- Lead link of the Software Factory circle
- The candidate's coach (project leader/lead link)
- the lead link of the candidate's coach (upper lead link)
- 2 team members of the candidate, chosen by the candidate. ( if you are not in the position to pick up two team members, explain why and proceed anyway)


### The Pitch

The pitch is a meeting of more or less 2 hours, with two phases:
- The candidate's pitch ( 30 minutes )
- The committee's assessment 

After the pitch of the candidate, the committee will express an evaluation in the following way:
* Technical Skills: coach + Architecture lead link + Software Factory lead link + team member
* Get Stuff Done: coach + team member
* Impact: coach + upper lead link
* Communication & Leadership: coach + upper lead link + team member

To get the promotion on each feature, you need to reach unanimity and the result will be given immediately after the pitch.

If you get not promoted, you will receive extended feedback about motivations and what you should focus on to get there.


### Compensation

The compensation must always be decided by a person that is possibly two levels ( of the ladder) above the promotion's candidate's target level.


