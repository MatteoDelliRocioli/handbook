# How to get certifications

As part of the benefits offered to employees, Agile Lab offers the opportunity to get professional tech certifications, covering costs and providing support along the preparation phase.

The {% role %}Training/Certification Coach{% endrole %} will help you to define feasible goals and to prepare for the exam.

At the beginning of the year, the CTO will set:
- a goal regarding the number and type of **new** certifications that should be obtained during the year by the whole company
- a goal regarding the minimum number and type of certifications that the company as a whole must **maintain** among the existing ones
These goals might be modified during the year to meet specific needs as opportunities arise.

The **new** certifications are considered "**priority 1**". You can find the certification basket inside the "Big Data" SharePoint at the path `Certifications/Certification Plan 202x`. People that will contribute to meet this achievement will be also rewarded, but only if the overall goal will be reached, like in a real team play.

The certifications that are needed to maintain a minimum number of certified people (eg to meet requirements for commercial partnerships and such) follow a slightly different process, but are treated in a similar way.

Finally, all other certifications are "**priority 2**".

#### Priority 1 certifications process:

1. For this kind of certification there is no need for an approval process, just check that there is still need for that specific certification and communicate to the Certification Coach your intention to prepare for it.
2. Meet the Certification Coach to define a study plan and a feasible exam date. 
3. If you need to buy specific material ( books, video courses ), drop an email to the Certification Coach for approval.
4. You can spend up to 24 working hours to prepare the exam, the remaining needed time should be your responsibility. ( Certifications are useful to the company, but in the end they are personal achievements ). In case you need a second try there will not be available working hours. 
5. Ask to People Operations to pay ( all costs will be covered ) and book your exam. For guidelines of how to request purchases or reimbursements, please consult [Spending Company Money](SpendingCompanyMoney.md).
6. Good Luck
7. Take the exam
8. Regardless if you fail or pass the exam:
- communicate the result to the Certification Coach
- update the status and date of your request in the certification plan
- for the sake of knowledge sharing, always report the exam's content (and possibly related tips and tricks) on the company's Big Data OneNote, in a dedicated section (check if are there already sections for your specific exam, in that case append in there your experience).
9. If you passed the exam, drop an email to the Certification Coach, the CTO and Social Publisher attaching certificates or digital badges, providing or not the authorization to publish the achievement on socials.
10. In case you fail the exam, is not a problem, we can have another try.
11. In case you fail again, there will not be other paid chances unless we face specific needs.

#### Priority 2 certifications process:

1. Drop an email to the {% role %}Agile Lab/CTO{% endrole %} specifying the reason and the motivation to get that specific certification.
2. In case the certification you want is similar to a "priority 1" one, this will take precedence.
3. In case the requested certification is congruent with the technical landscape we manage in the company and the price is reasonable, it will be approved.
4. Meet the Certification Coach to define a study plan and a feasible exam date.
5. You can't spend working hours to prepare such certification.
6. Ask to People Operations to pay ( all costs will be covered ) and book your exam. For guidelines about how to  make/request purchases or reimbursements, please consult [Spending Company Money](SpendingCompanyMoney.md).
7. Good Luck
8. Take the exam
9. Regardless if you fail or pass the exam:
- communicate the result to the Certification Coach
- update the status and date of your request in the certification plan
- for the sake of knowledge sharing, always report the exam's content (and possibly related tips and tricks) on the company's Big Data OneNote, in a dedicated section (check if are there already sections for your specific exam, in that case append in there your experience).
10. If you passed the exam, drop an email to the Certification Coach, the CTO and Social Publisher attaching certificates or digital badges, providing or not the authorization to publish the achievement on socials.
11. In case you fail, there will not be other paid chances.

# How to maintain the minimum certification levels

The certification coaches continuously track the expiration dates of all the certifications and the current certification numbers.

In case of certification expiration:
1. When the expiration date of a certification that must be maintained draws close (< 4 months), the certification owner will be contacted in order to organize the next steps
2. Depending on the type of certification and its relevance to the current career of the certification owner, the certification can be renewed as-is, another higher-level one might be chosen in its place, or it can be dropped if no longer relevant
3. Once the certification target has been defined, it is treated as a "priority 1" certification

In case a certified person leaves the company:
1. The certification coaches and the CTO decide if and how to compensate for the lost certifications
2. Either the "priority 1" certifications are extended to include the lost ones, or they are dropped if no longer relevant

# Where to find already available training material

In order to improve and speed up the search for training material already purchased or available, in the "Big Data" Sharepoint under the `Certifications` folder, you can eventually find a folder containing it. 

The convention is to specify the Fully Qualified Name of the exam (for example CCA 175 Spark and Hadoop Developer).

If you are aware of any previous material in another SharePoint folder, it is strongly recommended that you move it to the folder specified above.

# How to buy course material

If you need to buy course material related to certifications you can buy it yourself (with a personal account) and then log it as "Expenses" on ElapseIt. 

For guidelines about how to request/make purchases or reimbursements, please consult [Spending Company Money](SpendingCompanyMoney.md).

The limit for automatic approval is 50€, if the material costs more than 50€ you need to seek approval by writing to the Certification Coach

# Certification coaches

The current process followed by the certification coaches is hereby described.

#### Monitor the Bigdata/certifications teams channel

Monitor the `Bigdata/certifications` teams channel and provide feedback to everyone there

#### Begin of the year, a new goal is set by the CTO

When the `CTO` role sets a new certification goal for the year please create a new plan document at 
`sharepoint://BigData/Documents/Certifications/Certification Plan $YEAR`, you can clone the document of the past year.

Announce it by sending a mail to the `bigdata` mailing list (associated with the sharepoint group `BigData`) to reach every company member

#### Weekly check

Once a week remember to check the document located at `sharepoint://BigData/Documents/Certifications/Certification Plan 202x` and perform these actions:

* If there are unclaimed certifications (no one is actively working to take it) try to find candidates and repeat the announcements
* If there are approaching estimated exams dates please contact directly the candidate and ask if there are any blockers or things you can help with, if the candidate seems to be simply unmotivated or stressed propose switching to an "easier" certification or handing over the "reservation" to another more willing agile lab employee (do not blame the person but try to find out which part of the process is not working and why)
* If there are exams that have already been taken (if you heard about it or received partial information) but no success has been recorded or you did not receive the certificate remind the candidate of the missed parts of the process.

#### When a candidate contacts you

* Remind them of this document and to try to follow the process described in this document.
* Provide guidance on certifications you have taken yourself
* Facilitate the candidate by checking who already got the certification, make them talk to each other.

#### How to record achieved certifications

Other than the public certification plan `sharepoint://BigData/Documents/Certifications/Certification Plan 202x` please maintain the
`Engine` scoped document located at `sharepoint://engine/Documents/formazione/202x_AGILELAB_Censimento Formazione` with expiration, license number and other exams details, store the certificates received via mail under  `sharepoint://engine/Documents/formazione/certificates`

Always remember to give a big shoutout to certification achievers on the `thanks` team chanel to motivate everyone.