# Library

In AgileLab, we have some technical books, both physical and digital.

## Physical libraries
We have some physical books in company offices. 

You can find the list of available physical books in `sharepoint://Big Data/Documents/Library/Libreria Agile.xlsx`. This list provides visibility on all available physical books in AgileLab offices. This list helps you answering the following questions:
- What books are available in my office?
- What books are available in other offices?
- Is there a book that can help me with my learning path?
- Is there a book that can help me with a current task?

## How to buy new physical books
- Follow the book purchasing process on [SpendingCompanyMoney](SpendingCompanyMoney.md) page.
- After buying a new book, [update the list of available physical books](#how-to-update-the-list-of-available-physical-books).

## How to update the list of available physical books

The list of available physical books is maintained collaboratively. Please help us maintaining and update obsolete info.

When you order or share a new physical book in your office,
- access the physical book list in `sharepoint://Big Data/Documents/Library/Libreria Agile.xlsx`,
- in the `Current` tab, please add book title and office info in a new row.


## Digital library
You can find:
- the list of digital resources in `gitlab:AgileFactory/developers/awesome-resources`,
- some digital books in `sharepoint://Big Data/Documents/Library`.

## How to buy new digital books
- Follow the book purchasing process on [SpendingCompanyMoney](SpendingCompanyMoney.md) page.
- After buying a new book, [update the list of available digital books](#how-to-update-the-list-of-available-digital-books).

## How to update the list of available digital books

The list of available digital books is maintained by the {% role %}Software Factory/Mario Fusco (Internal repo maintainer){% endrole %}. 

To request a change or update to this list, please send an email to the person(s) that currently energize the {% role %}Software Factory/Mario Fusco (Internal repo maintainer){% endrole %}, using `[Agile Digital Library]` as subject prefix and attaching the file of the book.
