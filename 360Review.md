
In Agile Lab, after 7 years of performance measurements and many different evaluation systems, we understood that it is really hard to be fair. We value fairness and transparency so we can't live with that.

Also, our self-management culture is encouraging people to take accountabilities out of their comfort zone (accept new technical or leadership challenges, work in additional circles, OKR, etc...) but, when it comes to evaluating performance, most of it is coming from project activities, so people could be tempted to maximize their individual performance versus their impact at the company level, and this is something we don't want to happen. It would be the classical example of when an economic incentive is playing against the organization itself, known as the distraction/cancellation effect. 
We want to value adaptive behaviors, team working and we want people to have an impact at company level. All such behaviors are hard to measure, then it is unlikely that pay-for-performance systems will be fair.

Then no performance review is happening in Agile Lab and the bonus (economic incentive) is not linked to the individual performance, but it is linked to the overall company performance instead. We want people to act at their best because driven by play, purpose, and potential (ref. Total Motivation factor). We want people acting like shareholders, not individual performers.


At the end of each semester we have anyway the 360 review.


## 360 Review


**Everyone in the company can participate.**

360 reviews allow everyone to give and receive feedbacks in order to improve, this is a 360 because in this way we can build a more complete view about each of us and collect more useful feedbacks.
It is just another way to provide feedbacks, and because we value transparency **it is NOT anonymous and it must be actionable** in order to improve.

It is important to understand that people are not judging you but they are providing feedbacks. These are not right or wrong but they are just feedbacks, please listen to them.
When you provide feedbacks be responsible and owner of what you think and what you say, be candid and be ready to discuss more about your feedback in 1:1 with the specific person.

Here the skills composing the 360:

* Leadership Skills (Delegation, Ownership/Responsibility, Vision, Listening, Approachability, Coaching, Decision Making, Risk-Taking )
* Communication Skills ( Listening, Clarity, Speaking, Networking, Non-Verbal Behaviors, Openness, Energy, Giving Feedback, Receiving Feedback)
* Team Skills (Listening, Questioning, Helping, Participation, Peer Feedback, Reliability)
* Organization Skills ( Long term goals, Time management, and Attention to Detail)
* Creativity Skills (Problem Solving, Problem Identification, Inventiveness, Brainstorming, and Making connections )
* Interpersonal Skills (Empathy, Confidence, Stress management, Positivity, Group Work, Approachability, Enthusiasm, and Personal Appearance)
* Organizational Alignment (Alignment & Understanding of: Community, Values, Mission, Vision, Strategic Plan, and Processes)

You must provide your feedback to all the people you worked with during the past semester, no matter if you worked in the same team or you collaborated in a side project or in a circle, but the feedback should be based on some interaction. It is not needed to provide a vote on each skill, but only on the ones you are opinionated about.

Each skill can have a vote between 1 and 5 with the following meaning:

1. Unsatisfactory/Unacceptable: it should be 5% of people
1. Needs Improvements: individual is not doing a good job and needs to shape up immediately or the persone is new to a specific position and, while on track is not yet successful level of performance. It should be 10% of people
1. Meets Expectations/Solid Performer: It should be 60% of people that are really doing a great job inline with what the company demands
1. Exceeds Expectations/Above standard. It should be 20% of people
1. Distinguished/Outstanding. It should be 5% of people. This person performed his job for the whole period  at superior level, is not driven by episodes but some incredible achievement must be there

The "vote" should not be on an absolute scale, instead it should be based on the expectations you have with regard to the reviewed subject. For example, in case of engineering/technical dept. you should not grade a Eng I and an Eng IV with the same meter, because you expect very different things from them.

Each review must be completed with some direct feedback with the following format:

* Stop: what the person should stop to do
* Start: what the person should start to do
* Continue: what the person should continue to do

All the suggestions should be actionable, not vague messages like "stop sending mixed messages on certain issue" and no feedbacks like "you are great, I enjoy working with you" because they are not actionable in any way. Be specific, honest and constructive. No blaming.
An actionable feedback is "you can be overly confident, even aggressive, in advocating for a position. I felt this was the case when you were advocating for facility re-opening. You ask for opinions but it seems you are predetermined to get a certain outcome" or "you don't respond to emails from my team, it feels hierarchical and discouraging. Perhaps we need to establish more trust but I need you be more generous with your time and insights". These are good feedbacks, even if negative, because it is clear what they are referring , also with specific references and examples, and providing some suggestion on how to improve. 
A good feedback should be given with a positive intent, don't provide feedback in order to get frustration off your chest, intentionally hurting the other person. Address the problem and explain what behavior change could help the company or the individual, **not how it could help you**.

Each 360 review, when completed should be shared with your coach.
It could be really helpful for leaders to share back their 360 evaluations and show to the team their weakness and improvement needs. This will improve transparency ( not mandatory ).

In order to obtain a 360 you need to do the following steps:
- go into sharepoint BigData/ Performance review / 360 review template.
- open the link e clone the template ( using the top button ), renaming it with this naming convention -->  360 Review YYYYMM - Name Surname.
- copy the link (using the _Share_ button) and add it into the file "360 review - yyyy H1/H2". Then notify all the people you want to get feedback from, or tag them in the file (or in the dedicated channel "360 review", if existing). Remember: there is no link with the bonus here so be eager for feedback (also coming from circles and not from projects).
- once your review is completed and you got all the feedbacks, please share results with your coach. **( this step is only for Engine people )**

The process is the following.
- COO starts officially the process
- two days to clone the form and publish it
- then we have 2 weeks to fill in all the 360s because we want you to take the proper time to think about how to provide feedback and make them insightful for other people. Please, keep track of who wants to have feedback from you, and don't forget about it.
- Each coach starts to discuss them



# Other

Finally, filling the 360 review provides the perfect chance to give your feedback to the company about your learning path progression, if are there impediments, etc.

**When you share your review with your coach, please add some personal consideration, self assessment, and whatever you feel could be important to highlight at company level**



