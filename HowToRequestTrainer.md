# How to request a trainer for a training

Whenever a client requests training for which it is not possible to identify a suitable trainer in terms of skills, seniority or availability, the following process is initiated:
* A document is drafted with all the required training information (as specified below);
* This document is uploaded to SharePoint (Training section) so you can easily share it as a link via chat, email, or other channels;
* A Doodle is created where colleagues can apply as a trainer for that course;
* The request is disseminated by the training manager on the Teams channel `Training/c4training`.

# Information to be included in the training specification document

* Course title (defined by the training manager when not specified by the customer)
* Summary description of the course
* Programming languages, technologies or topics
* Language (Italian, English or other)
* End customer (where possible)
* Number and type of participants (if available)
* Detailed course schedule (if available)
* Dates and location (if available)
* Educational material availability and tutorials, with link to them
* Is special seniority required by the trainer or specific previous experience?

# How do I respond to the call and apply?

To apply, simply answer the Doodle that will be linked on Teams in the appropriate channel. If necessary (in cases where they have not been set by the end customer) dates will be proposed.

# Possible process improvements

* Create a searchable database of the provided trainings
* Create a database with trainer CVs (these are often requested by the customer for acceptance)
