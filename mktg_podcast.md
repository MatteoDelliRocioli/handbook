# Introduction

The diffusion of the events and workshops run by Agile Lab is important to make our competencies known and to share opinions; for this reason, in addition to the video broadcast, a channel for audio broadcast has been created.

# Process

On the channel, we publish the audio events where no slides are used so that can be understood without video support.

The channel can be found [here](https://anchor.fm/agile-lab) and it is automatically broadcasted on different platforms:
* [Breaker](https://www.breaker.audio/agile-lab)
* [Google Podcasts](https://www.google.com/podcasts?feed=aHR0cHM6Ly9hbmNob3IuZm0vcy81NmFkODdlYy9wb2RjYXN0L3Jzcw==)
* [Pocket Casts](https://pca.st/8fz1w714)
* [RadioPublic](https://radiopublic.com/agile-lab-6vKxqD)
* [Spotify](https://open.spotify.com/show/0HS9r2RqRqVlM6BDMH8KHL)

The following information is required to publish a new podcast:
* file in mp3 format
* title
* short description
* cover image size 
