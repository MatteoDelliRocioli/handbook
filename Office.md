All employees are assigned to a physical facility (an office) even if they work remotely most of the time.
If you need to buy something to work with, whether to use it in the office or at your home workstation, always ask the Facility Manager.

The Facility Manager will also be available to buy amenities and assets to improve the employees' work-life in the office.

# Hardware purchase and buyback

Each employee will receive a laptop and a screen during onboarding and can ask his/her Facility Manager for all additional hardware (cables, keyboard, mouse, etc).
During onboarding, each laptop must be registered in a census file by the receiver. After 3 years of use, an employee can ask the Buyer for the substitution of his/her laptop: the new laptop model and specifications are decided by Internal IT and updated frequently.
When an employee asks for a laptop upgrade he/she can ask to buy the previous laptop for personal use. This process of laptop redemption is regulated by the following rules:
*   The following rules apply **only for the laptop** assigned to each employee by the company.
*   After 3 years of usage (you can check this time in the census file) an employee can ask the Buyer to buy a new laptop. At that moment the employee can buy the laptop that is going to be substituted for personal use.
*   The employee can use the benefit and prize money to buy the old laptop (without the 30% discount since there is no VAT).
*   The price of the old laptop buyback is defined at the market price at the current date minus a 10% discount for the employee. A valuation of the market price will be performed by the employee by proposing an average of similar laptops on the used market (exact models or previous/following ones). The employee will send the list of links and evaluations to the Buyer that will check them before defining the buyback price.
*   Additional considerations on the price based on the state of the machine will be discussed case by case (e.g. you want to buy your old laptop even if it has some damaged components).
*   When the buyback is approved the employee must tell Internal IT that the laptop is no longer company property so they can apply GDPR un-enrollment policies.

# COVID-19 Office rules

During Covid-19 pandemic no mandatory presence in the office is required and we encourage remote working.
Accessing the office is still possible but there are some rules to be obverseved:
* Book your presence in the office: every office location has a spreadsheet where you can book your presence, in order to avoid crowding and keep traceability
* Always wear a mask in common areas
* Check your temperature at the office entrance

Presence in the office is regulated with respect to the region's "colour":
* *white* area: office will have 50% of available capacity
* *yellow* area: office will have 30% of available capacity
In these two cases, if there is no way to mantain social distancing, it will be accountability of the facility manager to introduce an *ad hoc* limitiation.
* *orange* and *red* area: office is closed

Furthermore, for those who are in a coworking environment, please check the local COVID-19 agreements and rules