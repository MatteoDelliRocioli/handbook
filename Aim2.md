Most of the things humans do are processed and performed by their brain, the source of computational power that is within the direct proximity of their limbs: where their own processing power is not enough to solve a problem, they can tap into knowledge that is located at a further location. In contrary, most mobile apps, IoT and other applications that work with AI must rely on processing power sitting in the cloud or at a datacenter at thousands of miles away.

This is where AI on the Edge plays a fundamental role. **AI on the Edge** means that AI algorithms are processed locally on a hardware device, allowing real time operations where timing to take decisions and actions matters, reducing costs for data communication, avoiding streaming and storing data or images in the cloud that makes your system vulnerable from a privacy perspective.

Our purpose is leveraging all those capabilities that AI on the Edge can offer to solve real world problems and help people to improve their life. This opens a wide range of opportunities: discover them with **Aim2**.

http://www.aim2.io/