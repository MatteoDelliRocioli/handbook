# Introduction

We use the articles a lot as a marketing tool with the goal of maintaining a high presence of the company on the major social channels, to advertise the skills present in the company, to share experiences and projects.

On social media it is planned the publication of:

* **POSTS**: quick publication for the sponsorship of events, the re-post of third-party articles considered interesting, company news such as certifications acquired, new recruitment, etc. ...
* **ARTICLES**: by articles we mean a technical and/or functional and/or commercial content that has been proposed, written and approved entirely among our people.

# Articles

The steps leading to the publication of a single article:

## Idea

Everyone in the company can submit a proposal for a new blog post. You can submit a proposal for an article you want to write, but you can also just suggest an idea for a new article that you think should be written. Article writers can use up to 8 working hours to produce content for our tech blog. Please talk with your team leader before using these hours to be sure that it will not impact project delivery.

Here follows our publication process:

* Creating a new issue on the `AgileFactory/marketing/articles` repository
* Use the `template blog.md` to fill the description of the issue with a summary of the proposed contents (tips: use the `agile-gitlab-cli` to open the new issue as follows `lab issue new-work-item -k blog "<title of the issue>"`)
* If you do not want to be the author of the suggested article, add the `missing author` label to the number

## Realization

According to their internal organization within the group, people with a `Content Owner` role will check the repository to evaluate the open issues. 

The following workflow shows the process Content Owners should follow to manage an article life cycle:

![workflow_content_owners](images/workflow_content_owners.svg)

### Writing Phase

In this phase, two different roles will collaborate in order to create an article: the Article Writer (**AW**) and the Content Owner (**CO**).

The goal of this phase is to:
- produce an article
- make sure that we can publish it on the majority of the platforms

In order to make the writing of the article as straightforward and simple as possible, the following sub-steps are delineated:
- Prerequisites
    - The AW **must** write a BIO on his Medium account, since it will displayed in the last part of the Article. In the BIO, please specify that you work in Agile Lab.

- Kickoff
The CO creates a meeting (max 1 hour) with the AW in order to discuss how the article should be written. The topics to be discussed (at least) should be:
- Structure of the article (sections, topics, etc.)
- Technologies to be used, along with their precise versions (if the article is a technical one)
- The goal of the article (should be technical and help other developers, should only be an informative article, etc.). This is very important since it determines the "type" of the article (more informative, more/less images, more/less text, etc.)

- Writing phase
    - The AW creates a private Medium draft, and links it in the issue. **This will be used by the publisher when the article is ready.**
    - The Article Writer begins to write the article directly on Medium. While writing it, due to technical limitations between some platforms (where to possibly cross-post articles), the AW **must** follow these guidelines:
        - Do not insert any incipit on your article (like "Hi, I'm < Author > and I work here, etc"). Your updated BIO on Medium is sufficient
        - Text: allowed in any case
        - Images: allowed in any case
        - Formulas (Latex, etc): allowed *only* through images
        - Code: Allowed (in a text format) *only* during the writing phase. You can use three backticks (\`) to enclose your code on Medium. If you have indentation problems, you can use also use [GISTs](https://gist.github.com/)
        - Tables: To be avoided as much as possible. If strictly necessary, allowed *only* through images
        - Other: Discuss with CO
    - The rule of thumb is: write the article on Medium as well as you can.
    - If needed, the AW and CO can organize quick calls (max 30 mins per week) to monitor the article's writing status
    - During the article's writing, the AW can contact the CO if a content review is needed
    - After the article has been written, the AW notifies the CO in order to begin the "final" review
    - All the working hours used by the article writer should be tracked on elapsed on "Agile Marketing" project, specifying the article you're working on in the notes.

- Review (done by the CO and AW)
    - Iteratively reviews the article with the AW
        - The reviews can directly be done on Medium. Select a portion of the text, and a note will appear there
    - Helps to polish the article on Medium

- Assembling (Phase 1) [done by AW]
    
    If the article contains code, the Article Writer:
    - Takes the source code, organize it in some source files and saves them in the [articles source code repository](https://github.com/agile-lab-dev/articles-code) mantained by Agile Lab. This repo contains a list of folders (one per article), and the associated source code files (one or more).
    - Opens a PR on the above repo. This PR will be managed by the CO after the WIP is removed
    - Converts the code from textual representation to a Carbon image in embedded a non-embedded form
        - Follow the instructions on the repository on how to generate those images.
        - *There is a small caveat with this method*: due to the rendering on Medium, code snippets with a small portion of code (say 2-3 lines) are not rendered well in a standard image, since it is bigger than it needs. Consider merging portions of code or make the image smaller (on Medium). In case of single code snippets, you can use the triple backticks (\`\`\`). In any case, the AW should decide if he wants to copy (or not) this code snippets into the source code repository.


- Assembling (Phase 2) and Polishing [done by the CO] 
    
    After that the article has been polished and it's in a final state:
    - Asks the AW to provide all the images in their original format (including code images)
        - In general, the CO takes everything needed to publish the article elsewhere
    - *Before* notifying the Publisher, the CO must be sure to:
        - Provide the Publisher:
            - a link to the Medium draft of the article, already polished and ready to be published. **Only an article with a draft link will be eligible for publishing.**
            - a ZIP file (attached to the issue) containing all the necessary material so that the article can be published on a variety of platforms:
                - Images (eventually including code images)
            - If the article contains code:
                - Link to the GitHub folder containing the source code (it the article contains code)
                - Be sure to close the PR on the [articles source code repository](https://github.com/agile-lab-dev/articles-code)

### Publish phase 
After being notified by the CO, the Publisher should:
- Contact the AW in order to synchronize the publishing on Medium (publish by AW + link the article in our tech blog)
- The text to be published on the other platforms should have the *same structure and content* of the article in the Medium draft
- Then, the Publisher should do the following changes (described in order):
    - If the article contains code, **when publishing to any platform other than Medium**, the Publisher must:
        - Use the code images provided by the CO
        - **Add a link to the respective GitHub folder in the last section of the article to be published**. For example:
        ```
        <article>

        The source code can be viewed on GitHub at the following link: <link> 
        ```
    - When publishing anywhere but Medium, the Publisher should add the following sections:
        - Incipit (before the article): **This article has been written originally by < Author >.**
    - Finally, in any case, after the article's contents (AND after the link to the source code - if applicable): **If you made it this far, you may be interested in other tech articles that you can find on our [site](https://www.agilelab.it/ideas-knowledge-base/). Stay tuned: more articles are coming!**
- Once published, update the Marketing metrics on Sharepoint. The publisher is also responsible for labelling the original issue with the label "Published".

## Planning

Planning of publications is done by people with Planner role and can be done from the day after the due date defined in the issue (Writer and Content Owner establish a due date as the date in which the article will be written and approved); the timing of publication is freely established according to marketing opportunities.

# Posts

Most of the posts published are the result of the work of the social publisher who independently decides on content, timing and channels.

As for the re-posting of third-party links, Content owners are asked weekly to select a couple of contents discussed in the technical chats in the company and to communicate them to the social publisher with a quick text. 
