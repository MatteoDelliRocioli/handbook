
Because in Agile Lab we are data professionals and we care about security and compliance, everyone should review and be compliant with the GDPR Company Policy document, available on BigData Sharepoint in the GDPR section.

In addition to this, everyone is in charge of checking their computer on a monthly base and delete all data that are not longer in use.  
