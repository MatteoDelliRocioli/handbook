# Introduction
Video content generates a lot of interest in the social world and that's why we are trying to increase more and more the publication of this kind of content.
Videos should be as short as possible, with impressive and attractive content even if watched without audio (as often users do).
Each of us should take the practice of filming or recording any event, meeting occasion, meeting, etc... that can be used as marketing material in the future. 
It's a good rule, if the video contains people external to Agilelab, to film them from behind or in any case so as not to violate privacy or request permission to publish.

# Process
The steps leading to the publication of a video only:

## Idea
Everyone in the company can submit a proposal for a new video. You can add issues for
* submit a proposal for a video you want to film 
* make public the addition on Stream of a video that has already been filmed 
* suggest an idea for a new video that you think should be filmed. 


The process follows:
* Create a new issue on the AgileFactory/marketing/videos repository
* Use the video.md template to fill in the title, the description, the minutes deemed significant (in case of long video as for an event), the Stream link and the content owner 
* If you don't want to be the author of the suggested video, add the "missing author" label 

    
# Realization
According to their internal organization within the group, people with a Content Owner role will check the repository to evaluate the open issues. Content Owners will identify the video recorder and together will define the "due date" which is intended as the recording plus approval for publication. The recorded video is published on Stream and the link added to the issue. Remember to add a Video Editor as video owner when the video is being upoloaded (check who is playing the Video Editor role on holaspirit). Once the author has completed the recording, the content owner should review it and approve its publication by closing the issue.

The content owners organize themselves to divide the present issues choosing a timing that allows to always have at least 6 videos in progress and at least 4 in delivery per month.

The implementation phase ends with the editing, which is done by the people with the role of video content generator.
 
# Planning
The planning of publications is done by people with a Planner role and can be done from the week following the due date written in the issue  (Recorder and Content Owner establish a due date that includes both recording a approving tasks/timings); the timing of publication is freely established according to marketing opportunities.

# Publication
The publication is the responsibility of the social publisher and is followed by a phase of measurement of retention by the social analyst.
Once published, the material will be saved on sharepoint in the Marketing/Video folder.
